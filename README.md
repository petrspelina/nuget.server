# Local and simple NuGet server #

This project implements a simple NuGet server application to be deployed on local application web server.

### Version info ###
* The current solution was prepared in VS2013 using .NET 4.5
* For .NET 4.6, read additional notes regarding version(s) of the NuGet.Server package


### Setting Up NuGet Server ###

* Download the solution with NuGetServer project 
```
#!
  https://petrspelina@bitbucket.org/petrspelina/nuget.server.git 
```
* Build the application (enable update of loaded NuGet packages to include latest fixes)
* Publish or deploy the application onto the target (local) web server
* Set up the application pool supporting ASP.NET 4.x
* In properties of **Packages** folder authorize the web server default user (IIS_IUSRS) to modify, read, write, read/execute, list 
* Authorize the access to ..\NuGetServer\Packages for default web server user in the file system
* WARNING: The simple server/app set up is designed for "internal" sites with a controlled access by other security means. Should this be deployed on a server with an unrestricted public access, the security measures has to be reviewed and hardened.  


### Testing the NuGet Server ###
* Open application's default page (or root directory)  on respective url (eg. http://localhost/NuGetServer/)
* The following info  should appear.
```
#!

You are running NuGet.Server v2.10.3.0

Click **here** to view your packages.

Repository URLs
In the package manager settings, add the following URL to the list of Package Sources:
http://localhost/NuGetServer/nuget
To enable pushing packages to this feed using the nuget command line tool (nuget.exe). Set the api key appSetting in web.config.
nuget push {package file} -s http://localhost/NuGetServer/nuget {apikey}
Adding packages
To add packages to the feed put package files (.nupkg files) in the folder C:\inetpub\wwwroot\NuGetServer\Packages

Click **here** to clear the package cache.

```
* Test hyperlink to view packages (it should not fail to execute)
* Test hyperlink to clear the package cache (it should not fail)

NOTE: If any of these tests fails, the likely cause is that the default web server user does not have required access to the Packages folder

### Deploying NuGet Packages ###
* the package(s) can be manually dropped into the Packages folder.  The NuGet Server then automatically takes care of all preparation steps to publish the NuGet packages
* the packages could be pushed to the repository using [nuget.exe tool](Link https://dist.nuget.org/index.html) as outline on the info page
```
#!
nuget push {package file} -s http://localhost/NuGetServer/nuget {apikey}
```

### Accessing NuGet packages ###
* add NuGet packages source URL into the package manager within visual studio

### References and sources ###
The construction of the project is heavily based on information within the following articles and sites:
* NuGet - general info - https://www.nuget.org/
* NuGet - local host - https://docs.nuget.org/create/hosting-your-own-nuget-feeds


## IMPORTANT: .NET 4.6 vs .NET 4.5 support ##
* if you're targeting .NET 4.6, download the latest version of NuGet.Server  2.11.2 or higher
* if you're targeting .NET 4.5, use the version loaded into this project - NuGet.Server 2.10.3 